function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementEnostavniTekstSmiley(sporocilo) {
  var x = $('<div style="font-weight: bold"></div>').append(sporocilo);
  $('#sporocila').append(x);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    
    var smiley = sporocilo.indexOf(":)");
    var wink = sporocilo.indexOf(";)");
    var like = sporocilo.indexOf("(y)");
    var kiss = sporocilo.indexOf(":*");
    var sad = sporocilo.indexOf(":(");
    
    if(smiley > -1 || wink > -1 || like > -1 || kiss > -1 || sad > -1){
      var indexOkl = sporocilo.indexOf("<");
      if(indexOkl > -1){
        sporocilo = sporocilo.split("<").join("&lt");
      }
      if(smiley > -1){
        sporocilo = sporocilo.split(":)").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png'>");
      }
      if(wink > -1){
        sporocilo = sporocilo.split(";)").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png'>");
      }
      if(like > -1){
        sporocilo = sporocilo.split("(y)").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png'>");
      }
      if(kiss > -1){
        sporocilo = sporocilo.split(":*").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png'>");
      }
      if(sad > -1){
        sporocilo = sporocilo.split(":(").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png'>");
      }
      
      divElementEnostavniTekstSmiley(sporocilo);

    }
  
    else $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    
    
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    $('#kanal').text(rezultat.kanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    
    var smiley = sporocilo.besedilo.indexOf(":)");
    var wink = sporocilo.besedilo.indexOf(";)");
    var like = sporocilo.besedilo.indexOf("(y)");
    var kiss = sporocilo.besedilo.indexOf(":*");
    var sad = sporocilo.besedilo.indexOf(":(");
    
    if(smiley > -1 || wink > -1 || like > -1 || kiss > -1 || sad > -1){
      if(sporocilo.besedilo.indexOf("<") > -1){
        sporocilo.besedilo = sporocilo.besedilo.split("<").join("&lt");
      }
      if(smiley > -1){
        sporocilo.besedilo = sporocilo.besedilo.split(":)").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png'>");
      }
      if(wink > -1){
        sporocilo.besedilo = sporocilo.besedilo.split(";)").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png'>");
      }
      if(like > -1){
        sporocilo.besedilo = sporocilo.besedilo.split("(y)").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png'>");
      }
      if(kiss > -1){
        sporocilo.besedilo = sporocilo.besedilo.split(":*").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png'>");
      }
      if(sad > -1){
        sporocilo.besedilo = sporocilo.besedilo.split(":(").join("<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png'>");
      }
      
      
      var nov = $('<div style="font-weight: bold"></div>').append(sporocilo.besedilo);
      $('#sporocila').append(nov);
    }else{
      var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
      $('#sporocila').append(novElement);
    }
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});